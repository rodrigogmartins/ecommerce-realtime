'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ProductSchema extends Schema {
  up () {
    this.create('products', (table) => {
      table.increments()
      table.string('name', 200).notNullable()
      table.integer('image_id').unsigned().notNullable()
      table.text('description').notNullable()
      table.decimal('price', 12, 2).unsigned().notNullable()
      table.timestamps()

      table.foreign('image_id')
        .references('id')
        .inTable('images')
        .onDelete('cascade')
    })

    this.create('image_product', table => {
      table.increments()
      table.integer('image_id').unsigned().notNullable()
      table.integer('product_id').unsigned().notNullable()

      table.foreign('image_id')
        .references('id')
        .inTable('images')
        .onDelete('cascade')


      table.foreign('product_id')
        .references('id')
        .inTable('products')
        .onDelete('cascade')
    })


    this.create('category_product', table => {
      table.increments()
      table.integer('category_id').unsigned().notNullable()
      table.integer('product_id').unsigned().notNullable()

      table.foreign('category_id')
        .references('id')
        .inTable('categories')
        .onDelete('cascade')


      table.foreign('product_id')
        .references('id')
        .inTable('products')
        .onDelete('cascade')
    })
  }

  down () {
    this.drop('category_product')
    this.drop('image_product')
    this.drop('products')
  }
}

module.exports = ProductSchema
