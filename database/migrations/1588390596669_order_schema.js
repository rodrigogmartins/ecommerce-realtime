'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class OrderSchema extends Schema {
  up () {
    this.create('orders', (table) => {
      table.increments()
      table.integer('user_id').unsigned().notNullable()
      table.decimal('total', 12, 2).unsigned().notNullable()
      table.enu('status', ['pending', 'cancelled', 'shipped', 'paid', 'finished'])
      table.text('description').notNullable()
      table.timestamps()

      table.foreign('user_id')
        .references('id')
        .inTable('users')
        .onDelete('cascade')
    })
  }

  down () {
    this.drop('orders')
  }
}

module.exports = OrderSchema
